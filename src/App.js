// import logo from './logo.svg';
import "./App.css";
import React from "react";
import { HashRouter, Routes, Route } from "react-router-dom";

import BMmusic from "./Components/BMmusic/BMmusic";
import BMjewel from "./Components/BMJewel/BMjewel";
import BMmega from "./Components/BMmega/BMmega";
import BMgalaxy from "./Components/BMgalaxy/BMgalaxy";
import BMgame from "./Components/BMgame/BMgame";
import BMlast from "./Components/BMlast/BMlast";
import Glowbg from "./Components/General_Components/Glowbg/Glowbg";
import MintPage from "./Components/MintZone/Mintpage/MintPage";
// import Glowbg from './Components/General_Components/Glowbg/Glowbg';

function App() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/bmmusic" element={<BMmusic />} />
        <Route path="/bmjewel" element={<BMjewel />} />
        <Route path="/bmmega" element={<BMmega />} />
        <Route path="/bmgalaxy" element={<BMgalaxy />} />
        <Route path="/bmgame" element={<BMgame />} />
        <Route path="/mintZone" element={<MintPage />} />
        <Route path="/" element={<BMlast />} />
      </Routes>
      {/* <BMmusic></BMmusic> */}
      {/* <BMjewel></BMjewel> */}
      {/* <BMmega></BMmega> */}
      {/* <BMgalaxy></BMgalaxy> */}
      {/* <BMgame></BMgame> */}
      {/* <BMlast></BMlast> */}
    </HashRouter>

    // <Glowbg></Glowbg>

    // <Glowbg></Glowbg>
  );
}

export default App;
