import React from "react";
import Navigation from "../General_Components/Navigation/Navigation";
import "./BMlast.css";
import lastGalaxy from "./images/lastGalaxy.png";
import linked from "./images/linked.png";
import twitter from "./images/twitter.png";
import youTube from "./images/youTube.png";
import ballon from "./images/ballon.png";
import lastVideo from "./images/lastVideo.mp4";
import lightVideo from "./images/lightVideo.mp4";
import spaceVideo from "./images/spaceVideo.mp4";
import sordVideo from "./images/sordVideo.mp4";
import starVideo from "./images/starVideo.mp4";
import sylinderVideo from "./images/sylinderVideo.mp4";
import Menu from "../General_Components/Menu/Menu";
import SideStrip from "../General_Components/SideStrip/SideStrip";
import { Link } from "react-router-dom";

const BMlast = () => {
  return (
    <section className="BMlast">
      <div className="main_section1">
        <Navigation />
        <Menu />
        <SideStrip />
        <div className="last_div">
          <div className="last_main">
            <div className="last_main_img">
              <video src={lastVideo} loop autoPlay="true" />
              {/* <img src={lastGalaxy}></img> */}
            </div>
            <div className="last_main_content">
              <div className="last_edit">
                <h1 className="content_heading">BMverse™</h1>
                <h2 className="content_discription">
                  The Decentralized Platforms Provider Generation ALPHA
                  <br />
                  Prerequisites in Web3
                </h2>
              </div>
              <div className="content_steps">
                <div className="step">
                  <h3>Community</h3>
                  <h4>Development Capability</h4>
                  <p>
                    The First Global Web3 Community For Designer Professionals.
                    Holders gain access to a global community consisting of the
                    most progressive individuals within the Game & Design
                    industry.
                  </p>
                </div>

                <div className="step">
                  <h3>Discover</h3>
                  <h4>unique Senses</h4>
                  <p>
                    Relying on the data obtained from the analysis of the
                    frequencies produced and received by the brain, we realized
                    that it is possible to provide gamers with the most
                    realistic sense of a virtual world . . .
                  </p>
                </div>

                <div className="step">
                  <h3>Next-Gen of</h3>
                  <h4>Interactive metaverse games</h4>
                  <p>
                    Man, innately, is a creator, enters the world, uses the
                    creation of others, grows, evolves, and provides what the
                    next generation needs.
                  </p>
                </div>
              </div>
              <Link style={{ textDecoration: "none" }} to="/bmgame">
                <h3 className="content_text">Start of the journey</h3>
              </Link>
            </div>
          </div>
          <div className="social_media">
            <div className="media">
              <a href="https://www.linkedin.com/company/bmverse/">
                <img src={linked} />
              </a>
            </div>
            <div className="media">
              <a href="https://twitter.com/bmverseio">
                <img src={twitter} />
              </a>
            </div>
            <a href="#" />
            <div className="media">
              <a href="https://www.youtube.com/channel/UC9_6umU_VkYZ2v6Rvl6BLMQ">
                <img src={youTube} />
              </a>
            </div>

            <div className="media">
              <a href="https://medium.com/@bmverseio">
                <img src={ballon} />
              </a>
            </div>
          </div>
        </div>

        <div className="last_text">
          <h1>BMCards</h1>
          <h2>THE MAGICAL POTION</h2>
        </div>
        <div className="light_video">
          <video src={lightVideo} loop autoPlay="true" />
        </div>

        <h2 className="heading_h2">
          ALL THE CHARACTERS WHO LIVE IN THE METAVERSE OF BMVERSE HAVE MAGICAL
          ABILITIES THAT CAN BE ACTIVATED BY USING AND BUYING THE BM CARDS
        </h2>

        <div className="video_div1">
          <video src={spaceVideo} loop autoPlay="true" />
          <video src={sylinderVideo} loop autoPlay="true" />
        </div>

        <h3 className="heading_h3">
          EVERYCHARACTER IN MEGA COLLECTION WILLBEUPGRADED IN 3STEPS.THE SECOND
          ABILITYOFTHECHARACTERS CAN BEACTIVATED IN 3STAGES AND BY BUYING CARD
          GAMES CALLED MAGIC POTION CARDS.MAGIC POTIONS CAN BE TRADED IN THE
          MARKET PLACE OF BMVERSE
        </h3>

        <div className="video_div1">
          <video src={starVideo} loop autoPlay />
          <video src={sordVideo} loop autoPlay />
        </div>
      </div>
    </section>
  );
};

export default BMlast;
