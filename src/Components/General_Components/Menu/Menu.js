import React from "react";
import "./Menu.css";
import linked from "./images/linked.png";
import twitter from "./images/twitter.png";
import youTube from "./images/youTube.png";
import ballon from "./images/ballon.png";
import openSea from "./images/openSea.png";
import discord from "./images/discord.png";
import { Link } from "react-router-dom";

const Menu = () => {
  return (
    <section className="Menu">
      <div class="hamburger-menu">
        <input id="menu__toggle" type="checkbox" />
        <label class="menu__btn" for="menu__toggle">
          <span />
        </label>

        <div class="menu__box">
          {/* <li>
            <a class="menu__item" href="#">
              Home
            </a>
          </li>
          <li>
            <a class="menu__item" href="#">
              About
            </a>
          </li>
          <li>
            <a class="menu__item" href="#">
              Team
            </a>
          </li>
          <li>
            <a class="menu__item" href="#">
              Contact
            </a>
          </li>
          <li>
            <a class="menu__item" href="#">
              Twitter
            </a>
          </li> */}

          <div className="menu_1">
            <div className="open_sea">
              <a href="#">
                <img src={openSea} />
              </a>
              <h2> OPENSEA </h2>
            </div>

            <div className="discord">
              <a href="#">
                <img src={discord} />
              </a>
              <h2> DISCORD</h2>
            </div>

            <div className="menu_links">
              <a href="#">Home</a>
              <Link to="/mintZone">Mint Page</Link>
              {/* <a href="#">Mint Page</a> */}
              <a href="#">collection</a>
              <a href="#">Pages</a>
              <a href="#">Blog</a>
            </div>

            <div className="copy_right">
              <h2>Copyright 2022 - Designed and developed by BMverse</h2>
            </div>

            <div className="menu_social">
              <a href="https://twitter.com/bmverseio">
                <img src={twitter} />
              </a>
              <a href="https://www.linkedin.com/company/bmverse/">
                <img src={linked} />
              </a>
              <a href="https://www.youtube.com/channel/UC9_6umU_VkYZ2v6Rvl6BLMQ">
                <img src={youTube} />
              </a>
              <a href="https://medium.com/@bmverseio">
                <img src={ballon} />
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Menu;
