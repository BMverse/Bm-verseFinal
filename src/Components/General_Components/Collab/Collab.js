import React from "react";
import "./Collab.css";

const Collab = () => {
  return (
    <section className="collab">
      <h1>Collaborate</h1>
      <div className="main_collab">
        <div className="collab_div1">
          <div className="radio_div">
            <div>
              <input type="radio" id="html" name="fav_language" value="HTML" /> {" "}
              <label htmlFor="html">Founder</label>
            </div>
            <div>
              <input type="radio" id="css" name="fav_language" value="CSS" /> {" "}
              <label htmlFor="css">Developer</label> {" "}
            </div>
            <div>
              <input
                type="radio"
                id="javascript"
                name="fav_language"
                value="JavaScript"
              />
                <label htmlFor="javascript">Community</label>
            </div>
          </div>
          <div className="input_divc">
            <input type="text" id="fname" name="fname" placeholder="Name" />
            <input
              type="text"
              id="sname"
              name="sname"
              placeholder="Sure Name"
            />
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Ens or eMail"
            />
            <input
              type="text"
              id="conversance"
              name="conversance"
              placeholder="Conversance"
            />
            <input
              type="text"
              id="talent"
              name="talent"
              placeholder="Innate talent"
            />
            <input
              type="text"
              id="demand"
              name="demand"
              placeholder="Demand Type"
            />
            <input
              type="text"
              id="twitter"
              name="twitter"
              placeholder="Linkedin - Twitter"
            />
            <input
              type="text"
              id="github"
              name="github"
              placeholder="Github - Discord"
            />
          </div>
        </div>

        <div className="collab_div2">
          <h3>What is the fiction about?</h3>
          <textarea rows="20" col="10" />
          <input type="submit" />
        </div>
      </div>
    </section>
  );
};

export default Collab;
